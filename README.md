## Description

[AngularJSリファレンス](http://www.amazon.co.jp/AngularJS%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9-%E6%B1%A0%E6%B7%BB-%E6%98%8E%E5%AE%8F/dp/4844336681) のサンプルコードを実際に書いて動かしてみるRepositoryです

## Requirement

* IE9
* FireFox
* Goole Chrome
* Safari

## References

* [AngularJS](https://angularjs.org/)
* [angularjs-eclipse](https://github.com/angelozerr/angularjs-eclipse)
* [AngularJSリファレンス](http://www.amazon.co.jp/AngularJS%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9-%E6%B1%A0%E6%B7%BB-%E6%98%8E%E5%AE%8F/dp/4844336681)

## Author

[Yusuke Kawatsu](https://bitbucket.org/meg_mog_mog)
